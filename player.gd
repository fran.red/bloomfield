extends Person

class_name Player

# todo: delete export once https://github.com/godotengine/godot/issues/23316
export(int) var player_walk_speed = 50

var input_enabled = true
var interaction_picker = null 

# for the starting sequence
# hack: there's probably a better place to put this
var scripted_intro = false
var target: Node2D

signal choose_interaction(interactions)
signal moved_to_start_convo_mark

func _ready() -> void:
	add_to_group('player')
	walk_speed = player_walk_speed

func _process(delta: float) -> void:
	# let Person parent handle _process
	.process(delta)

	if not input_enabled:
		return

	if Input.is_action_just_pressed("ui_select"):
		if is_streaking:
			stop_streaking()
		elif not is_talking:
			attempt_interact()

	if input.two_button_mode:
		if Input.is_action_just_pressed("ui_cancel"):
			if is_sitting:
				stand_up()

# provide this override for parent
func get_velocity() -> Vector2:
	# calculate velocity from input
	var velocity := Vector2()

	# in these states/cases, let Person parent class handle velocity
	if is_sitting or is_talking or is_streaking:
		velocity = .get_velocity()
	elif scripted_intro:
		var pos := get_position()
		var target_pos := target.get_position()

		if pos.distance_to(target_pos) < 1:
			emit_signal('moved_to_start_convo_mark')
			scripted_intro = false
			target = null
		else:
			velocity = (target_pos - pos).normalized() * .25
	# basically, if we're walking around in a freeroam state
	elif input_enabled:
		if Input.is_action_pressed("ui_right"):
			velocity.x += 1
		if Input.is_action_pressed("ui_left"):
			velocity.x += -1
		if Input.is_action_pressed("ui_up"):
			velocity.y += -1
		if Input.is_action_pressed("ui_down"):
			velocity.y += 1
	return velocity

func end_convo() -> void:
	# wait a frame, so we don't chain convos
	yield(get_tree(), 'idle_frame')
	.end_convo()

func start_scripted_intro(target: Node2D) -> void:
	self.target = target
	scripted_intro = true

func choose_interaction(interactions: Array) -> Interactions.Interaction:
	# todo: bring up a menu of options to choose from 
	input_enabled = false
	emit_signal('choose_interaction', interactions)
	var idx = yield(interaction_picker, 'interaction_selected')
	interaction_picker.deactivate()
	input_enabled = true

	if idx == -1:
		return null
	else:
		print('idx is %s' % idx)
		print('interaction is %s' % interactions[idx].get_type())
		return interactions[idx]
	
func show_highlight():
	idle_anim = 'idle-highlight'
	run_anim = 'run-highlight'

func hide_highlight():
	idle_anim = 'idle'
	run_anim = 'run'

