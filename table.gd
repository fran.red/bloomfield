extends StaticEntity
class_name Table

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var is_full := false

const MAX_SITTING := 4
var num_sitting := 0

# sent when player stands up
signal stand_up


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

# returns persons seat number
func sit(person) -> int:
	if not person:
		print('(table): error: person is null')
		return -1
	var seat_num := -1
	if not $seat_top_left.is_full:
		$seat_top_left.sit(person)
		seat_num = 0
	elif not $seat_top_right.is_full:
		$seat_top_right.sit(person)
		seat_num = 1
	elif not $seat_bot_left.is_full:
		$seat_bot_left.sit(person)
		seat_num = 2
	elif not $seat_bot_right.is_full:
		$seat_bot_right.sit(person)
		seat_num = 3
	num_sitting += 1
	return seat_num

func unsit(seat: int) -> void:
	if seat < 0 or seat >= 4:
		print('(table): error: only have 4 seats')
	match seat:
		0:
			$seat_top_left.unsit()
		1:
			$seat_top_right.unsit()
		2:
			$seat_bot_left.unsit()
		3:
			$seat_bot_right.unsit()
	num_sitting -=1

func get_seat(seat: int): # -> Seat
	match seat:
		0:
			return $seat_top_left
		1:
			return $seat_top_right
		2:
			return $seat_bot_left
		3:
			return $seat_bot_right
		_:
			print('(table.gd): invalid seat num')
			return null


func is_full() -> bool:
	return num_sitting >= MAX_SITTING

func stand_up(person): # Person
	emit_signal("stand_up")
