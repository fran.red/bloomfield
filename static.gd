extends StaticBody2D

# a thing in the world that can't be moved
class_name StaticEntity

# Called when the node enters the scene tree for the first time.
func _ready():
	set_z_index(get_position().y)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
