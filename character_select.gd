extends Container

class_name CharacterSelect

const CharacterTable = preload('res://character_table.gd')

onready var list = $container/container/character_list
onready var prompt = $container/prompt

var confirm_phase = false

const select_prompt := 'X to select'
const confirm_prompt := 'Z to cancel\nX to confirm'

var char_table

signal character_selected(character)

func _init() -> void:
	pass

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	list.connect('item_selected', self, '_on_list_item_selected')

	char_table = CharacterTable.new()
	for character in char_table.playable_chars:
		if character.is_playable:
			list.add_icon_item(character.anim.get_frame('idle', 0))


	# begin char select at first option
 	list.select(0)
	# and highlight it
	list.set_item_icon(0,
		char_table.playable_chars[0].anim.get_frame('idle-highlight', 0))


	# let character list recieve keyboard commands
	list.grab_focus()
	

func _process(delta: float) -> void:
	if Input.is_action_just_pressed('ui_select'):
		var selected_idx : int = list.get_selected_items()[0]
		emit_signal('character_selected', 
				char_table.playable_chars[selected_idx])
		print('you selected item', selected_idx)

func _on_list_item_selected(item_idx: int) -> void:
	print('item %s selected!' % item_idx)

	var character = char_table.playable_chars[item_idx]
	list.set_item_icon(item_idx,
		character.anim.get_frame('idle-highlight', 0))

	# clear the highlight from the left item
	if item_idx > 0:
		var prev_char = char_table.playable_chars[item_idx - 1]
		list.set_item_icon(item_idx - 1,
			prev_char.anim.get_frame('idle', 0))

	# clear the highlight from the right item
	if item_idx < char_table.playable_chars.size() - 1:
		var next_char = char_table.playable_chars[item_idx + 1]
		list.set_item_icon(item_idx + 1,
			next_char.anim.get_frame('idle', 0))





