class_name Interactions

class Interaction:
	var choose_text := 'some interaction'

	func handle(person, world) -> void:
		print('person interacted with world')

	func get_type() -> String:
		return 'BaseInteraction'

class CancelInteractionChoice extends Interaction:
	func _init() -> void:
		choose_text = 'cancel'

	func handle(person, world) -> void:
		# we do nothing, interaction picker exits automatically
		return

	func get_type() -> String:
		return 'CancelInteractionChoice'

class TalkToPerson extends Interaction:
	var talkee: Person = null

	# yeah i hate this too but you know what:q
	# https://github.com/godotengine/godot/issues/27136
	var Npc = load('res://npc.gd')
	
	func _init(p_talkee: Person) -> void:
		talkee = p_talkee
		choose_text = 'talk to %s' % talkee.name
	
	func handle(person, world) -> void:
		# todo: allow talkee to decline the conversation
		print('%s talking to %s' % [person.name, talkee.name])

		# do nothing if talkee is in conversation
		if talkee.is_talking:
			return

		if person is Npc:
			person.add_goal(Npc.HaveConvoWithPerson.new(talkee))
		if talkee is Npc:
			talkee.add_goal(Npc.HaveConvoWithPerson.new(person))

		# in case they're yelling
		person.stop_yelling()
		talkee.stop_yelling()

		person.is_talking = true
		talkee.is_talking = true

		person.talkee = talkee
		talkee.talkee = person

		person.set_global_position(
				talkee.talkee_pos.get_global_position())
		person.set_flip_h(true)
		talkee.set_flip_h(false)

		var dialogue_ui = world.dialogue_ui_scene.instance()
		dialogue_ui.init(person, talkee, world)
		world.add_child(dialogue_ui)

		# hacky
		dialogue_ui.start_convo()

	func get_type() -> String:
		return 'TalkToPerson(%s)' % talkee.name

class BeginConsumeItem extends Interaction:
	var item: HeldItem = null

	func _init(p_item: HeldItem) -> void:
		item = p_item

		var verb: String
		if item.item.is_drink:
			verb = 'drink'
		else:
			verb = 'eat'
		choose_text = '%s %s' % [verb, item.item.name]

	func handle(person, world) -> void:
		item.start_consuming()
		return

	func get_type() -> String:
		return 'BeginConsumeItem(%s)' % item.name

class Sit extends Interaction:
	# todo: generalize this to other sittables
	var sittable: Table = null

	func _init(p_sittable: Table) -> void:
		sittable = p_sittable
		choose_text = 'sit at %s' % sittable.name

	func handle(person, world) -> void:
		if sittable.is_full():
			return
		var seat_num := sittable.sit(person)
		person.sit()
		world.people_seated_table[person] = seat_num

	func get_type() -> String:
		return 'Sit(%s)' % sittable.name

class StandUp extends Interaction:
	func _init() -> void:
		choose_text = 'stand up'

	func handle(person, world) -> void:
		person.stand_up()

	func get_type() -> String:
		return 'StandUp'

class RunAndYell extends Interaction:
	func _init() -> void:
		choose_text = 'run and yell'

	func handle(person, world) -> void:
		print('want to run and yell!')
		person.start_streaking()

	func get_type() -> String:
		return 'RunAndYell'

