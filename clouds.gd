extends Sprite

# one-off script for animating the clouds in the backgroundd

onready var start_x = get_position().x
onready var rate := int(60 / speed)

var frames: int = 0
var speed: float = 1.2

var loop_width: int = 320

func _ready():
	pass # Replace with function body.

func _process(delta: float) -> void:
	frames += 1
	var pos := get_position()
	var new_x = pos.x

	# loop sprite back after <loop_width> pixels
	if new_x < start_x - loop_width:
		new_x = start_x

	# every <rate> frames, move cloudes over a pixel
	if (frames % rate) == 0:
		new_x -= 1

	set_position(Vector2(new_x, pos.y))
		

