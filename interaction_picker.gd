extends Node2D

var active = false

signal interaction_selected(idx) # (interaction)

func activate(interactions: Array, player_pos: Vector2) -> void:
	if interactions.empty():
		print('(interaction_picker): error: no interactions')

	# wait a frame to buffer between the input of pressing select
	yield(get_tree(), 'idle_frame')

	for interaction in interactions:
		$list.add_item(interaction.choose_text)

	# place picker on screen
	# (nb: the picker's anchor is on its bottom-left corner)

	# if the picker would go offscreen, flip to the left
	var right_offset := 4
	if player_pos.x + $list.get_rect().size.x + right_offset > 255:
		# place to left
		var left_offset: int = $list.get_rect().size.x + 4
		set_global_position(
			Vector2(player_pos.x - left_offset, player_pos.y))

	# by default, just place it a little to the right of the player
	else:
		# place to the right
		set_global_position(
			Vector2(player_pos.x + right_offset, player_pos.y))

	$list.grab_focus()
	$list.select(0)
	show()
	active = true

func deactivate() -> void:
	# clear it
	$list.clear()
	hide()
	$list.release_focus()
	active = false

func _process(delta: float) -> void:
	if active:
		if Input.is_action_just_pressed('ui_select'):
			emit_signal('interaction_selected', 
				$list.get_selected_items()[0])

		if input.two_button_mode:
			if Input.is_action_just_pressed('ui_cancel'):
				emit_signal('interaction_selected', -1)

