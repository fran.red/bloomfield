extends DialogueUIBehavior

class_name DialogueUI

onready var speech_bubble = $speech_bubble

var person_a: Person
var person_b: Person
var world

signal dialogue_completed(convo) # DialogueUI

func init(a: Person, b: Person, p_world) -> void:
	person_a = a
	person_b = b
	world = p_world
	name = 'convo-%s-%s' % [a.name, b.name]

func _ready():
	$dialogue_runner.dialogue_ui = self

	if person_a is Player:
		# player's convo should always be in front of other convos
		speech_bubble.set_z_index(speech_bubble.get_z_index() + 1)

func dialogue_started() -> void:
	print('dialogue started!')

func dialogue_complete() -> void:
	person_a.end_convo()
	person_b.end_convo()
	person_a.stop_bobbing_head()
	person_b.stop_bobbing_head()

	add_child(speech_bubble)
	speech_bubble.hide()

	queue_free()
	print('dialogue completed')

# line is a Dialogue.Line
# todo: add type once it doesn't break it. something about inner classes
func run_line(line) -> void:
	var split : Array = line.text.split(': ')
	if split.size() != 2:
		print('(dialogue_ui) error: badly formatted line')
		return
	
	var person_str : String = split[0]
	var text : String = split[1]

	# hack: don't yet have a full working yarnspinner in gdscript
	if person_str == 'COMMAND':
		yield(run_cmd(text.split(' ')), 'completed')
		return


	var talker: Person
	var listener: Person
	var left := false
	match person_str:
		# hack: want a more robust way of detecting characters
		'a':
			talker = person_a
			listener = person_b
			left = false
		'b':
			talker = person_b
			listener = person_a
			left = true

		_:
			print('(dialogue_ui) error: invalid person')
			return

	print('making %s say %s' % [person_str, text])

	# places the speech_bubble
	say(talker, text, left)
	talker.start_bobbing_head()
	listener.stop_bobbing_head()

	yield(get_tree(), 'idle_frame')
	# could also do something like:
	# yield(self, 'player_input_recieved)
	# hack: should have some general 'run_next_line' signal that we 
	# wait on

	if person_a.name == 'player':
		# wait for player input
		while not Input.is_action_just_pressed("ui_select"):
			yield(get_tree(), 'idle_frame')
	else:
		yield(get_tree().create_timer(2.0), "timeout")

func run_cmd(args : Array) -> void: # Strings
	print('running command for ', args[0])

	# first arg is the function
	match args[0]:
		'get':
			if args.size() != 3:
				print('invalid number of arguments for get')
			var item: String = args[2]
			person_a.hold_item(item_table.table[item])
		_:
			print('(dialogue_ui) error: invalid argument')
			return

	yield(get_tree(), 'idle_frame')

# hack: might want an overall dialogue controller thing
func start_convo():
	# hack: every person has a default dialogue that they use when 
	# talked to
	$dialogue_runner.start_dialogue('%s-default' % person_b.name)

# person: the person that is saying this line
func say(person: Person, line: String, left: bool) -> void:
	speech_bubble.text.set_text(line)

	if test.per_character_speech_color:
		speech_bubble.text.add_color_override('font_color', 
			person.character.color)
	else:
		if person is Player:
			speech_bubble.text.add_color_override('font_color', 
				# yellow (the less green one)
				# Color("f0d472"))
				# greenish-yellow 
				Color("e3d245"))
		else:
			speech_bubble.text.add_color_override('font_color', 
				# white
				Color("f9f5ef"))

	var person_pos = person.get_global_position()
	if person.is_sitting:
		print('person sitting yus')
		person_pos = world.get_person_seated_pos(person)
		# todo: specify whether person is on left or right,
		# given where they're sitting in table
	else: # we assume person must be free roaming
		person.add_child(speech_bubble)


	# different heuristics for placing the speech bubble based on if 
	# the character is standing on the right or left in convo
	if left:
		speech_bubble.place_left(person_pos, true)
	else:
		speech_bubble.place_right(person_pos, true)
