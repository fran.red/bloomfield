extends Node

# our input layer

# are we using 2 buttons or 1 button?
var two_button_mode = true

signal selected
signal canceled

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	if Input.is_action_pressed('ui_select'):
		emit_signal('selected')

	if Input.is_action_pressed('ui_cancel'):
		emit_signal('canceled')
