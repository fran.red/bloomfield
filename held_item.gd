extends AnimatedSprite

class_name HeldItem

var item: Resource = null
var charges_remaining := 0

# only really used by NPC AI
var being_consumed = false
var consume_timer: Timer = null

signal charge_consumed(item)
signal want_consume_charge(item)
signal consumed

func hold(p_item: Resource) -> void:
	item = p_item
	charges_remaining = item.charges
	being_consumed = false
	set_sprite_frames(item.anim)
	play()

func drop() -> void:
	item = null
	charges_remaining = 0
	being_consumed = false
	set_sprite_frames(null)

func is_full() -> bool:
	return item != null

func is_consumed() -> bool:
	return charges_remaining <= 0

func is_holding(p_item: Resource) -> bool:
	return item and item.name == p_item.name

func start_consuming() -> void:
	if being_consumed:
		return
	being_consumed = true
	emit_signal('want_consume_charge', self)
	
	
# called (usually by person owner) when the charge has been succesfully consued
func consume_charge() -> void:
	if is_consumed():
		return

	charges_remaining -= 1
	emit_signal('charge_consumed', item)

	if is_consumed():
		stop()
		set_frame(0)
		emit_signal('consumed')
	else:
		# if we have more charges to consume, start cooldown
		consume_timer = Timer.new()
		consume_timer.wait_time = 3 
		consume_timer.one_shot = true
		consume_timer.connect('timeout', self, 
				'_on_consume_timer_timeout')
		add_child(consume_timer)
		consume_timer.start()
	
# this means we want to take another bite
func _on_consume_timer_timeout() -> void:
	print('consume timer timeout')
	emit_signal('want_consume_charge', self)


