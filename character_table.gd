extends Resource

# holds all the characters (see character Resource)

# idea: generalize this into a general "resource table" thing
# idea: instead of dynamically loading from a dictionary, have option of 
# manually specifying names in code and then loading that? idk

var all_chars := []
var playable_chars := []
var resource_dir := 'res://characters'

func _init():
	print('loading char table')
	var dir := Directory.new()
	var err := dir.open(resource_dir)
	if err != OK:
		print('(character_table.gd): invalid path')
		return

	dir.list_dir_begin()
	var char_file := dir.get_next()
	while char_file != '':
		if char_file == '.' or char_file == '..':
			char_file = dir.get_next()
			continue

		# do something
		# parse the name out
		print('char_file: ', char_file)
		
		var split := char_file.split('.')
		if split.size() != 2:
			print('(character_table): invalid file name')
			char_file = dir.get_next()
			continue

		var name = split[0]

		all_chars.append(load('%s/%s' % [resource_dir, char_file]))
		
		char_file = dir.get_next()

	dir.list_dir_end()

	for character in all_chars:
		if character.is_playable:
			playable_chars.append(character)







