extends AnimatedSprite

var item: HeldItem
var charge_being_consumed := false

func place(p_item: HeldItem) -> void:
	if not p_item.item:
		return

	item = p_item

	set_sprite_frames(item.item.anim)
	item.connect('consumed', self, '_on_item_consumed')

	if not item.is_consumed():
		play()

func unplace(item: HeldItem) -> void:
	set_sprite_frames(null)
	item.disconnect('consumed', self, '_on_item_consumed')
	item = null
		
func _on_item_consumed() -> void:
	stop()
	set_frame(0)

# func _on_person_consume_anim(item) -> void:
# 	if item is p_item:
# 		hide()
# 
# func _on_consume_anim_finished() -> void:
# 	show()
