extends Node

# holds all the items (see item Resource)

# idea: generalize this into a general "resource table" thing
# idea: instead of dynamically loading from a dictionary, have option of 
# manually specifying names in code and then loading that? idk

var table := {}
var resource_dir := 'res://items'

func item_to_area(item: Resource) -> Area2D:
	var person: Person = get_node('../main').get_node(item.source_person_path)
	if not person:
		print('(item_table.gd): no source person for item ', item.name)
	return person.interactable_area

func item_to_person(item: Resource) -> Person:
	var person: Person = get_node('../main').get_node(item.source_person_path)
	if not person:
		print('(item_table.gd): no source person for item ', item.name)
	return person

func _init():
	print('loading item table')
	var dir := Directory.new()
	var err := dir.open(resource_dir)
	if err != OK:
		print('(item_table.gd): invalid path')
		return

	dir.list_dir_begin()
	var item_file := dir.get_next()
	while item_file != '':
		if item_file == '.' or item_file == '..':
			item_file = dir.get_next()
			continue

		# do something
		# parse the name out
		print('item_file: ', item_file)
		
		var split := item_file.split('.')
		if split.size() != 2:
			print('(item_table): invalid file name')
			item_file = dir.get_next()
			continue

		var name = split[0]

		var item := load('%s/%s' % [resource_dir, item_file])
		item.name = name
		table[name] = item
		
		item_file = dir.get_next()

	dir.list_dir_end()






