extends StaticEntity

class_name Door

func _ready() -> void:
	pass


func open() -> void:
	$anim.set_frame(1)
	$collide_shape.set_disabled(true)

func close() -> void:
	$anim.set_frame(0)
	$collide_shape.set_disabled(false)
