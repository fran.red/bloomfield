extends Node2D

onready var people := get_tree().get_nodes_in_group('people')
onready var char_sel_container := $char_sel_container

const dialogue_ui_scene := preload('res://dialogue_ui.tscn')
const char_select_scene := preload('res://character_select.tscn')

# keeps track of who's seated at the table and their seat number
var people_seated_table := {} # Person -> int

var char_select : CharacterSelect

export var DEBUG := false
export var TWO_BUTTON_MODE := false
export var PER_CHARACTER_SPEECH_COLOR := false

var temp_char = preload('res://characters/fran.tres')

enum GameState {SPLASH, TITLE, CHAR_SELECT, NORMAL}
var game_state = GameState.SPLASH

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# set some constants for testing
	input.two_button_mode = TWO_BUTTON_MODE
	test.per_character_speech_color = PER_CHARACTER_SPEECH_COLOR

	# hook up signals
	for person in people:
		person.connect('attempted_interact', self,
				'_on_person_attempted_interact')
		person.connect('stood_up', self,
				'_on_person_stood_up')
		person.connect('started_streaking', self,
				'_on_person_started_streaking')

	# initialize areas singleton
	areas.exit_left_area = $exit_left_area
	areas.exit_right_area = $exit_right_area
	areas.table_area = $table/interactable_area

	# initiliaze the game world
	$player.input_enabled = false
	$player.interaction_picker = $interaction_picker
	$player.connect('choose_interaction', self, 
			'_on_player_choose_interaction')
	$player.connect('moved_to_start_convo_mark', self, 
			'_on_player_moved_to_start_convo_mark')

	char_select = char_select_scene.instance() as CharacterSelect

	
	# streaks to 
	# $philip.add_goal(Npc.Exit.new(true))
	# $philip.add_goal(Npc.Streak.new(areas.exit_right_area))

# 
	if DEBUG:
		# skip the intro
		print('we in debug mode')
		game_state = GameState.NORMAL
		$player.input_enabled = true
		$player.character = load('res://characters/peter.tres')
		$player.apply_character_resource($player.character)
		$title.queue_free()

		# DEBUG

	else:
		display_quote_briefly()
		$player.set_position($player_start_pos.get_position())

	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta) -> void:

	# DEBUG
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		# $table.sit(temp_char)
		pass
	if Input.is_mouse_button_pressed(BUTTON_RIGHT):
		# $table.unsit(0)
		pass

	match game_state:
		GameState.TITLE:
			if Input.is_action_pressed('ui_select'):
				print('freeing title')
				$title.queue_free()
				game_state = GameState.CHAR_SELECT
				$char_sel_container.add_child(char_select)
				char_select.connect('character_selected', 
						self, 
						'_on_character_selected')
		GameState.NORMAL:
			if ($player.is_roaming() and 
				not gather_possible_interactions(
					$player).empty()):
				$player.show_highlight()
			else:
				$player.hide_highlight()

func display_quote_briefly():
	$black_splash.show()
	var t = Timer.new()
	t.set_wait_time(4)
	t.set_one_shot(true)
	t.connect('timeout', self, '_on_splash_timer_timeout')
	self.add_child(t)
	t.start()


func play_start_sequence() -> void:
	# open door, revealing player
	$door.open()

	# move player outside
	$player.start_scripted_intro($player_end_pos)
	$fran.add_goal(Npc.Exit.new(true))
	$fran.add_goal(Npc.MoveToArea.new($fran_intro_convo_mark))

	# wait 1 second
	var t = Timer.new()
	t.set_wait_time(1)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, 'timeout')
	t.queue_free()

	$door.close()

	$player.input_enabled = true


# character is a character Resource
func _on_character_selected(character) -> void:
	# print('selected ', character.name)
	$player.character = character
	$player/anim.set_sprite_frames(character.anim)
	$char_sel_container.queue_free()
	play_start_sequence()

func _on_person_attempted_interact(person) -> void:
	var interactions := gather_possible_interactions(person)

	if interactions.empty():
		print('DEBUG: NO INTERACTIONS')
		return

	# if just one interaction available, do that interaction
	if interactions.size() == 1:
		interactions[0].handle(person, self)
		return

	# else, we ask person choose between interactions
	print('more than 1 choice!')

	if not input.two_button_mode:
		# add the option to cancel out of the interaction picker
		interactions.append(Interactions.CancelInteractionChoice.new())

	# explanation of next few lines:
	# choose_interaction() sometimes calls yield() (i.e. when the player
	# needs to choose the interaction). when it does, it returns a 
	# GDScriptFunction, and we wait on it to eventually return an
	# Interaction
	# if choose_interaction() /doesn't/ call yield, it'll just return a
	# Interaction, which we don't wait on
	# i couldn't think of a cleaner way to do this

	# todo: statically type this. idk why it's not working, but i get a 
	# "Trying to assign value of type..." error and it sucks
	var interaction # : Interactions.Interaction
	var ret = person.choose_interaction(interactions)

	var coroutine := ret as GDScriptFunctionState
	if coroutine:
		interaction = yield(ret, 'completed')
	else:
		interaction = ret

	if interaction:
		interaction.handle(person, self)

func gather_possible_interactions(person) -> Array:
	var interactions := []


	if person.is_sitting:
		# can talk to other people at the table
		for sitter in people_seated_table:
			if not sitter == person:
				interactions.append(Interactions.TalkToPerson.new(sitter))

		# person may be holding items that they can interact with
		if person.left_item.is_full():
			if person.left_item.being_consumed:
				print('todo: option to stop consuming')
			else:
				interactions.append(Interactions.BeginConsumeItem.new(
					person.left_item))

		if person.right_item.is_full():
			if person.right_item.being_consumed:
				print('todo: option to stop consuming')
			else:
				interactions.append(Interactions.BeginConsumeItem.new(
					person.right_item))

		# the person can stand up
		interactions.append(Interactions.StandUp.new())
	# gather possible interactions from interactables in range
	else: # person is walking (not sitting or talking)
		var areas_in_range : Array = person.interact_area.get_overlapping_areas()
		for area in areas_in_range:
			# only InteractableAreas are assured to have a
			# parent that is interactable
			var interactable_area := area as InteractableArea
			if not interactable_area:
				continue

			var interactable = area.get_parent()

			var table := interactable as Table
			if table:
				interactions.append(Interactions.Sit.new(table))
				continue


			var talkee := interactable as Person
			if (talkee and 
				# talkee can't be self
				talkee.name != person.name and
				not talkee.is_talking):
				interactions.append(Interactions.TalkToPerson.new(talkee))
				continue


	# character specific interactions
	if person.character.char_name == 'philip':
		interactions.append(Interactions.RunAndYell.new())

	return interactions

func _on_player_choose_interaction(interactions) -> void:
	var player_pos = $player.get_global_position()
	if $player.is_sitting:
		player_pos = get_person_seated_pos($player)

	$interaction_picker.activate(interactions, player_pos)

func get_person_seated_pos(person: Person) -> Vector2:
	var seat = $table.get_seat(
			people_seated_table.get(person))
	print('sitting at seat %s' % seat.name)
	return seat.get_global_position()

func _on_person_stood_up(person) -> void:
	# todo: don't assume person is sitting at table
	# would probably involve some data structures / functions for
	# finding the person efficiently
	var seat_num = people_seated_table.get(person)
	if seat_num == null:
		print('(world): error, %s should be sitting?' % person.name)
		return
	var seat = $table.get_seat(seat_num)
	seat.unsit()
	person.set_global_position(seat.stand_up_pos.get_global_position())
	people_seated_table.erase(person)

func _on_person_started_streaking(streaker) -> void:
	print('we got a streaker!')
	for person in people:
		if person is Npc and person != streaker:
			person.add_goal(Npc.LookAtTarget.new(streaker))


func _on_timer_timeout() -> void:
	if not DEBUG:
		$rawb.add_goal(Npc.Exit.new(false))
		$rawb.add_goal(Npc.BuyItem.new(item_table.table['coffee']))

func _on_timer2_timeout() -> void:
	if not DEBUG:
		$sara.add_goal(Npc.Exit.new(true))
		$sara.add_goal(Npc.ConsumeItem.new(item_table.table['taco']))

func _on_splash_timer_timeout() -> void:
	$black_splash.show()
	$black_splash.queue_free()

	var t = Timer.new()
	t.set_wait_time(2)
	t.set_one_shot(true)
	t.connect('timeout', self, '_on_display_title_timeout')
	self.add_child(t)
	t.start()

func _on_display_title_timeout() -> void:
	game_state = GameState.TITLE
	$title.show()

func _on_player_moved_to_start_convo_mark() -> void:
	game_state = GameState.NORMAL
	Interactions.TalkToPerson.new($fran).handle($player, self)

	
	
	


