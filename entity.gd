extends KinematicBody2D

# base class for a thing that can exist in the world
class_name Entity

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta: float) -> void:
	# this makes it so that this thing is drawn in the right order
	# depending on what's in front of it.
	# things that are "lower" on the screen are more in the foreground
	$anim.set_z_index(get_position().y)

