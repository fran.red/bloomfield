extends Node
# just a singleton for being able to retrieve important areas
# todo: maybe combine this and the npc AI into one AI singleton

var exit_left_area: Area2D
var exit_right_area: Area2D
var table_area: Area2D

# todo: maybe have a find_open_seat() func? but maybe we want that in world?
