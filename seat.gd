extends AnimatedSprite
class_name Seat

export(bool) var front

onready var stand_up_pos := $stand_up_pos

var is_full := false
var person: Person = null

# Called when the node enters the scene tree for the first time.
func _ready():
	connect('animation_finished', self, '_on_consume_anim_finished')
	connect('animation_finished', $left_item, '_on_consume_anim_finished')
	connect('animation_finished', $right_item, '_on_consume_anim_finished')

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

# character is a character resource
func sit(p_person: Person) -> void:
	if is_full:
		print('(seat): error: seat already full')
	person = p_person
	is_full = true

	set_sprite_frames(person.character.anim)
	set_idle_anim()

	$left_item.place(person.left_item)
	$right_item.place(person.right_item)

	person.connect('consume_anim', self, '_on_person_consume_anim')
	person.connect('consume_anim', $left_item, '_on_person_consume_anim')
	person.connect('consume_anim', $right_item, '_on_person_consume_anim')
	connect('animation_finished', person, '_on_finish_consume_anim')


func unsit() -> void:
	set_sprite_frames(null)
	is_full = false

	person.left_item.disconnect('charge_consumed', self,
			'_on_item_charge_consumed')
	$left_item.unplace(person.left_item)

	person.right_item.disconnect('charge_consumed', self,
			'_on_item_charge_consumed')
	$right_item.unplace(person.right_item)

	person = null

func play_consume_anim(item: ItemResource) -> void:
	if (item.is_drink):
		set_animation('table_front_drinking')
	else:
		set_animation('table_front_eating')
	set_frame(0)
	play()

func set_idle_anim() -> void:
	if front:
		set_animation('table_front_idle')
	else:
		set_animation('table_back')
	stop()

func _on_person_consume_anim(item: ItemResource) -> void:
	if item == $left_item.item:
		$left_item.hide()
		$left_item.charge_being_consumed = true
	elif item == $right_item.item:
		$right_item.hide()
		$right_item.charge_being_consumed = true

	play_consume_anim(item)

func _on_consume_anim_finished() -> void:
	if $left_item.charge_being_consumed:
		$left_item.show()
		$left_item.charge_being_consumed = false
	elif $right_item.charge_being_consumed:
		$right_item.show()
		$right_item.charge_being_consumed = false
	set_idle_anim()

