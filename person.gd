extends KinematicBody2D

class_name Person

export var walk_speed: float = 100
export var character: Resource

onready var interact_area = $interact_area
onready var interactable_area = $interactable_area
onready var left_item = $left_item as HeldItem
onready var right_item = $right_item as HeldItem
onready var talkee_pos = $talkee_pos

# todo: document the item charge consuming thing bc it's a whole thing
# todo: make this a list/queue of items instead of hardcoding to two
var item_currently_consuming_charge: HeldItem = null
var item_waiting_for_consume_charge: HeldItem = null

# person is sitting down somewhere
var is_sitting := false

# person is engaged in conversation with somebody
var is_talking := false

# person is yelling something
var is_yelling := false

# other person that you're currently talking to
var talkee: Person

# these are used to switch between highlighted or not animations
var idle_anim := 'idle'
var run_anim := 'run'

# character specific states/vars

# streaking
var is_streaking := false
var streak_velocity := Vector2(0, 0)
signal started_streaking(person)

# if person is neither sitting nor talking, then they're chilling, roaming in 
# the world


signal started_convo(person_a, person_b)
signal attempted_interact(person)
signal stood_up(person)
signal consume_anim(item)

func _ready() -> void:
	add_to_group('people')
	$left_item.connect('want_consume_charge', self, 
			'_on_item_want_consume_charge')
	$right_item.connect('want_consume_charge', self, 
			'_on_item_want_consume_charge')
	apply_character_resource(character)

func _process(delta: float) -> void:
	# so that people are occluded by things correctly
	set_z_index(get_position().y)

func apply_character_resource(character: Resource) -> void:
	$anim.set_sprite_frames(character.anim)

func process(delta: float) -> void:
	if not is_sitting:
		var velocity := get_velocity()
		draw_person(velocity)
		move_and_collide(velocity * delta * walk_speed)


# this is mostly a virtual func that should be overridden by children
func get_velocity() -> Vector2:
	if is_streaking:
		return streak_velocity
	else:
		return Vector2(0, 0)

func draw_person(velocity: Vector2) -> void:
	# change animation and flip sprite if needed
	if velocity.length() == 0:
		$anim.animation = idle_anim
	else:
		$anim.animation = run_anim
		if velocity.x > 0:
			set_flip_h(false)
		elif velocity.x < 0:
			set_flip_h(true)

# shortcut to check this state
func is_roaming() -> bool:
	return not is_sitting and not is_talking

func has_item(item: Resource) -> bool:
	return $left_item.is_holding(item) or $right_item.is_holding(item)

func get_held_item(item: Resource) -> HeldItem:
	if $right_item.is_holding(item):
		return $right_item as HeldItem
	if $left_item.is_holding(item):
		return $left_item as HeldItem
	return null

func end_convo() -> void:
	print('ending convo for %s' % name)
	talkee = null
	is_talking = false

# recursively sets h flip for all important child sprites
func set_flip_h(flip: bool) -> void:
	$anim.set_flip_h(flip)
	$left_item.set_flip_h(flip)
	$right_item.set_flip_h(flip)

	# hacky
	if flip:
		$anim.set_offset(Vector2(-1, -3))
		$right_item.set_offset(Vector2(-4, -3))
		$left_item.set_offset(Vector2(-1, -3))
	else:
		$anim.set_offset(Vector2(1, -3))
		$right_item.set_offset(Vector2(1, -3))
		$left_item.set_offset(Vector2(4, -3))
	

# returns true if item was successfully held
func hold_item(item : Resource) -> bool:
	# hands are full
	if $left_item.is_full() and $right_item.is_full():
		return false
	# right hand free
	elif not $right_item.is_full():
		$right_item.hold(item)
		return true
	# left hand's free ~and your right's in a grip~
	elif not $left_item.is_full():
		$left_item.hold(item)
		return true

	# should never happen I think?
	print('hold_item(): something went wrong')
	return false

# hand: false for left, true for right
func drop_item(hand : bool) -> void:
	# todo: create actual Hand objects that each person has 2 of?
	if hand:
		$right_item.drop()
	else:
		$left_item.drop()

func play_consume_anim(item: ItemResource):
	emit_signal('consume_anim', item)

# consumes the charge from the held item, or queues it up if busy
func _on_item_want_consume_charge(item: HeldItem) -> void:
	print('%s want consume charge' % item.name)
	if item_currently_consuming_charge:
		item_waiting_for_consume_charge = item
	else:
		item_currently_consuming_charge = item
		play_consume_anim(item.item)


# this gets called when person (self) finishes a consume animation, but 
# also when a person is sitting, the seat will connect its 
# animation_finished signal to this
func _on_finish_consume_anim() -> void:

	# we should only ever have 2 items ready to consume charge
	# what if there's a bug, you say? to you i say, if there's a bug 
	# then there's a bug
	if item_currently_consuming_charge:
		item_currently_consuming_charge.consume_charge()
		item_currently_consuming_charge = null

	# if one item is waiting for charge to be consumed, do it
	if item_waiting_for_consume_charge:
		item_currently_consuming_charge = item_waiting_for_consume_charge
		item_waiting_for_consume_charge = null
		play_consume_anim(item_currently_consuming_charge.item)
	
func attempt_interact() -> void:
	emit_signal('attempted_interact', self)

func choose_interaction(interactions: Array): # -> Interactions.Interaction
	# if not overridden, just choose a "random" one
	return interactions[0]

func sit() -> void:
	is_sitting = true
	hide()
	remove_child(interactable_area)
	$collide_shape.set_disabled(true)

func stand_up() -> void:
	is_sitting = false
	show()
	add_child(interactable_area)
	$collide_shape.set_disabled(false)
	emit_signal('stood_up', self)

func start_bobbing_head() -> void:
	$anim.set_speed_scale(4)

func stop_bobbing_head() -> void:
	$anim.set_speed_scale(1)

func start_yelling() -> void:
	is_yelling = true
	$speech_bubble.show()
	$speech_bubble.text.set_text('aaaaaaaaaaaaaaaaaa')
	var facing_left = $anim.is_flipped_h()
	print('================== facing_left is %s' % facing_left)
	if facing_left:
		$speech_bubble.place_right(get_global_position(), false)
		streak_velocity = Vector2(-1, 0)
	else:
		print('so placing "left"...')
		$speech_bubble.place_left(get_global_position(), false)
		streak_velocity = Vector2(1, 0)

func stop_yelling() -> void:
	is_yelling = false
	$speech_bubble.hide()

# if you're wondering why we have this layer between yelling and streaking,
# it's because players and npc AIs control things differently
func start_streaking() -> void:
	is_streaking = true

	emit_signal('started_streaking', self)
	# start yelling
	start_yelling()

func stop_streaking() -> void:
	stop_yelling()
	is_streaking = false


