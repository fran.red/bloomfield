extends Person
class_name Npc

# todo: make less hacky
var gave_item: bool = false
var start_pos: Vector2
var target: Npc
var velocity: Vector2

var consume_timer: Timer = null

# Goals
class Goal:
	# requirements for being able to execute this state
	var reqs := []

	# interaction that would fulfill this goal
	var fulfiller_interaction := ''

	# whether we've accomplished what this state was trying to do
	# if so, this state will be popped off the stack
	var done := false

	# what the npc should be doing in the current frame
	# subclass is also responsible for assigning done to true here 
	# when the goal has been accomplished
	# in general, want to check for done-ness before executing
	func execute(npc: Npc) -> void:
		pass

class Idle extends Goal:
	func execute(npc: Npc) -> void:
		npc.velocity = Vector2(0, 0)

	func get_type() -> String:
		return 'Idle'


class ConsumeItem extends Goal:
	var item_type : Resource = null
	var item : HeldItem = null

	func _init(p_item_type: Resource) -> void:
		item_type = p_item_type
		reqs = [HasItem.new(item_type), PedSitting.new()]

	func execute(npc: Npc) -> void:
		# hacky way to get the item in question
		if item == null:
			item = npc.get_held_item(item_type)

		if item.is_consumed():
			done = true
			return

		if item.being_consumed:
			return

		item.start_consuming()

	func get_type() -> String:
		return 'ConsumeItem(%s)' % item_type.name


class BuyItem extends Goal:
	var item : Resource = null
	var source_person : Person = null

	func _init(p_item: Resource) -> void:
		# todo: convert item into the reqs
		item = p_item
		source_person = item_table.item_to_person(item)
		var area: Area2D = item_table.item_to_area(item)
		if not area:
			print('(npc.gd): no area for item')

		reqs = [InArea.new(area)]
		fulfiller_interaction = Interactions.TalkToPerson.new(
				source_person).get_type()

	func execute(npc: Npc) -> void:
		if npc.has_item(item_table.table[item.name]):
			done = true
			return

		npc.velocity = Vector2(0, 0)
		# todo: this should be better/more robust
		# 1) should maybe split BuyTaco into 2 states like
		#    TalkToTacoPerson or something (hard)
		# 2) should not hardcode person being talked to
		# 3) should just be interact() instead of start_convo()
		if not npc.is_talking:
			npc.attempt_interact()


	func get_type() -> String:
		return 'BuyItem(%s)' % item.name


class HaveConvoWithPerson extends Goal:
	var talkee : Person = null

	func _init(p_talkee: Person) -> void:
		talkee = p_talkee

	func execute(npc: Npc) -> void:
		if not (npc.is_talking and npc.talkee == talkee):
			done = true
			return
		
		# todo: hide personal speech bubble if yelling lol

		npc.velocity = Vector2(0, 0)

	func get_type() -> String:
		return 'HaveConvoWithPerson(%s)' % talkee.name

class MoveToArea extends Goal:
	var area: Area2D
	var target: Vector2

	func _init(p_area: Area2D) -> void:
		area = p_area
		target = p_area.get_global_position()
		reqs = [PedWalking.new()]

	func execute(npc: Npc) -> void:
		if npc.interact_area.overlaps_area(area):
			done = true
			return

		var pos := npc.get_global_position()

		npc.velocity = (target - pos).normalized()

	func get_type() -> String:
		return 'MoveToArea(%s)' % area.get_parent().name

class SitTable extends Goal:
	func _init() -> void:
		# hacky: todo: generalize this to be able to sit in other places
		reqs = [InArea.new(areas.table_area)]
		fulfiller_interaction = Interactions.Sit.new(
				areas.table_area.get_parent()).get_type()

	func execute(npc: Npc) -> void:
		if npc.is_sitting:
			done = true
			return
		npc.velocity = Vector2(0, 0)
		npc.attempt_interact()

	func get_type() -> String:
		return 'SitTable'

class Walk extends Goal:
	func _init() -> void:
		# todo
		reqs = []

	func execute(npc: Npc) -> void:
		# todo: should check for other states
		if not npc.is_sitting and not npc.is_talking:
			done = true
			return
		npc.stand_up()

	func get_type() -> String:
		return 'Walk'

class Exit extends Goal:
	var dir := ''

	func _init(right: bool) -> void:
		if right:
			dir = 'Right'
			reqs = [InArea.new(areas.exit_right_area)]
		else:
			dir = 'Left'
			reqs = [InArea.new(areas.exit_left_area)]

	func execute(npc: Npc) -> void:
		npc.queue_free()
		if not npc:
			done = true

	func get_type() -> String:
		return 'Exit%s' % dir

# streak to the specified area
class Streak extends Goal:
	var area: Area2D
	var target: Vector2

	func _init(p_area: Area2D) -> void:
		area = p_area
		target = p_area.get_global_position()
		reqs = [PedWalking.new(), IsYelling.new()]

	func execute(npc: Npc) -> void:
		if npc.interact_area.overlaps_area(area):
			done = true
			return

		# todo: make this less hacky and unify it more with how player
		# does it...
		if not npc.is_streaking:
			npc.is_streaking = true
			npc.emit_signal('started_streaking', npc)

		var pos := npc.get_global_position()
		npc.velocity = (target - pos).normalized()

	func get_type() -> String:
		return 'Streak(%s)' % area.get_parent().name

class Yell extends Goal:
	func _init() -> void:
		pass

	func execute(npc: Npc) -> void:
		if npc.is_yelling:
			print('YUP WE YELLING')
			done = true
			return

		else:
			npc.start_yelling()

	func get_type() -> String:
 		return 'Yell'

class LookAtTarget extends Goal:
	var target = null

	func _init(p_target) -> void:
		target = p_target

	func execute(npc: Npc) -> void:
		if not is_instance_valid(target):
			done = true
			return
		
		npc.velocity = Vector2(0, 0)

		var npc_x = npc.get_global_position().x
		var target_x = target.get_global_position().x
		if npc_x < target_x:
			npc.set_flip_h(false)
		else:
			npc.set_flip_h(true)

	func get_type() -> String:
 		return 'LookAtTarget(%s)' % target.name

# a requirement that the npc must fulfill to be in some state
class Req:
	# returns the 'fulfiller' goal should fulfill this requirement
	func get_fulfiller() -> Goal:
		return null

	# checks if the requirement is fulfilled by the npc
	func check(npc: Npc) -> bool:
		return false

class HasItem extends Req:
	var item : Resource = null

	func _init(p_item: Resource) -> void:
		item = p_item

	func get_fulfiller() -> Goal:
		return BuyItem.new(item)

	func check(npc: Npc) -> bool:
		return npc.has_item(item)

	func get_type() -> String:
		return 'HasItem(%s)' % item

class InArea extends Req:
	var area: Area2D = null

	func _init(p_area: Area2D) -> void:
		area = p_area

	func get_fulfiller() -> Goal:
		return MoveToArea.new(area)

	func check(npc: Npc) -> bool:
		return npc.interact_area.overlaps_area(area)

	func get_type() -> String:
		return 'InArea(%s)' % area.get_parent().name


class PedSitting extends Req:
	func get_fulfiller() -> Goal:
		return SitTable.new()

	func check(npc: Npc) -> bool:
		return npc.is_sitting

	func get_type() -> String:
		return 'PedSitting'

class PedWalking extends Req:
	func get_fulfiller() -> Goal:
		return Walk.new()

	func check(npc: Npc) -> bool:
		return not npc.is_sitting and not npc.is_talking

	func get_type() -> String:
		return 'PedWalking'

class IsYelling extends Req:
	func get_fulfiller() -> Goal:
		return Yell.new()

	func check(npc: Npc) -> bool:
		return npc.is_yelling

	func get_type() -> String:
		return 'IsYelling'

var state_stack := [Idle.new()]

var debug_name = 'poop'

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	start_pos = get_position()

func _process(delta):
	# 1) look at the top of the state stack. this is our current state
	var state: Goal = state_stack.front() # Goal
	if name == debug_name:
		print('curr NPC state: ', state.get_type())
		print('state stack is:')
		for state in state_stack:
			print(state.get_type())

	if state.done:
		state_stack.pop_front()
		if name == debug_name:
			print('fulfilled!')
		return

	# 2) are the reqs for the state met?
	if name == debug_name:
		print('checking requirements...')
	for req in state.reqs:
		# if there is a requirement that isn't fulfilled, push the
		# state needed to fulfill it onto the stack
		if name == debug_name:
			print('checking req ', req.get_type())
		if not req.check(self):
			var subgoal = req.get_fulfiller()
			if name == debug_name:
				print('not met! pushing state ', 
						subgoal.get_type())
			state_stack.push_front(subgoal)
			return

	if name == debug_name:
		print('all reqs met for state ', state.get_type())
		print('state stack is:')
		for state in state_stack:
			print(state.get_type())
	
	if name == debug_name:
		print('will now execute')
	state.execute(self)

	# parent class (person) handles movement/animations etc.
	.process(delta)


# should return a normalized vector (length == 1)
func get_velocity() -> Vector2:
	return velocity

func is_consuming(item: HeldItem) -> bool:
	return item.being_consumed

func add_goal(goal: Goal) -> void:
	state_stack.push_front(goal)

func choose_interaction(interactions: Array):
	# todo: choose based on current Goal
	print('%s the npc is choosing interaction' % name)
	var goal = state_stack.front()
	for interaction in interactions:
		if interaction.get_type() == goal.fulfiller_interaction:
			print('we got a hit!')
			return interaction
	return null

