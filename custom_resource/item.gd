extends Resource

class_name ItemResource

export(String) var name
export(SpriteFrames) var anim
export(NodePath) var source_person_path
export(int) var charges
# if true, you drink this item. if false, you eat it
export(bool) var is_drink

