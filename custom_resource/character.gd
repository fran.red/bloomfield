extends Resource

# character resource

class_name CharacterResource

export(String) var char_name
export(SpriteFrames) var anim
# used for dialogue text color
export(Color) var color

# whether the character can be selected to be played as
export(bool) var is_playable

