extends Node2D

# controller for person's speech bubble
onready var text = $text

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# when person is on "right" side in convo
func place_right(person_pos: Vector2, onscreen: bool) -> void:
	# doing this hide() here forces the speech bubble's automatic
	# resizing, which we need for placing the bubble correctly etc.
	hide()
	show()

	text.set_align(Label.ALIGN_LEFT)
	var offset = Vector2(-4, -19)
	print('person_pos: %s' % person_pos)
	set_global_position(person_pos + offset)

	# make sure the text stays onscreen 
	var right_edge = (get_global_position().x + 
			text.get_size().x)
	if onscreen and right_edge > 255:
		var diff = right_edge - 255
		var bubble_pos = get_global_position()
		set_global_position(
       			Vector2(bubble_pos.x - diff, bubble_pos.y))

# when person is on "left" side in convo
func place_left(person_pos: Vector2, onscreen: bool) -> void:
	# doing this hide() here forces the speech bubble's automatic
	# resizing, which we need for placing the bubble correctly etc.
	hide()
	show()

	text.set_align(Label.ALIGN_RIGHT)
	var offset = Vector2(3 - text.get_size().x, -19)
	set_global_position(person_pos + offset)

	# make sure the text stays onscreen 
	var left_edge = get_global_position().x
	if onscreen and left_edge < 1:
		print('must reposition')
		var diff = left_edge - 2
		var bubble_pos = get_global_position()
		set_global_position(Vector2(bubble_pos.x - diff, 
			bubble_pos.y))

